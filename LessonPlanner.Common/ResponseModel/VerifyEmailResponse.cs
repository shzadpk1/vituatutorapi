﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonPlanner.Common.ResponseModel
{
    public class VerifyEmailResponse
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }

        public string ClassCode { get; set; }
    }
}
