﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonPlanner.Common.ResponseModel
{
    public class UserSignInResponse
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }

        public bool isEmailVerified { get; set; }
        public bool isPhoneVerified { get; set; }
    }
}
