﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonPlanner.Assemblers
{
    public class GetTrueFalseQuizzesByMainTopicIDDto
    {
        public string QuestionType { get; set; }
        public long TrueFalseQuestionID { get; set; }
        public long MainTopicID { get; set; }
        public long GradeID { get; set; }
        public long SubjectID { get; set; }
        public string TrueText { get; set; }
        public string FalseText { get; set; }
	}
}
