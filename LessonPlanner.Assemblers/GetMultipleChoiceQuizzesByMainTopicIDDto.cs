﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonPlanner.Assemblers
{
    public class GetMultipleChoiceQuizzesByMainTopicIDDto
    {
        public string QuestionType { get; set; }
        public long MultipleChoiceQuestionID { get; set; }
        public long MainTopicID { get; set; }
        public long GradeID { get; set; }
        public long SubjectID { get; set; }
        public string NoOfOption { get; set; }
        public string OptionOneText { get; set; }
        public string OptionTwoText { get; set; }
        public string OptionThreeText { get; set; }
        public string OptionFourText { get; set; }
        public long AnswerID { get; set; }
        public int AnswerOptionNo { get; set; }
        public string AnswerOptionText { get; set; }
	}
}
