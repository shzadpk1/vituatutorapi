﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonPlanner.Assemblers
{
    public class GetFillBlankQuizzesByMainTopicIDDto
    {
        public string QuestionType { get; set; }
        public long FillBlankQuestionID { get; set; }
        public long MainTopicID { get; set; }
        public long GradeID { get; set; }
        public long SubjectID { get; set; }
        public string QuestionText { get; set; }
        public string AnswerText { get; set; }
        public long AnswerID { get; set; }
	}
}
